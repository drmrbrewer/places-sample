package com.example.myplacessampleapplication;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;

import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";
    private static final int AUTOCOMPLETE_REQUEST_CODE = 9768;
    private static final String PLACES_API_KEY = "INSERT_YOUR_KEY_HERE";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // ----- try also AutocompleteActivityMode.OVERLAY -----
                AutocompleteActivityMode mode = AutocompleteActivityMode.FULLSCREEN;

                launchAutoComplete(mode);

                Snackbar.make(view, "have launched autocomplete activity", Snackbar.LENGTH_SHORT)
                        .setAction("Action", null).show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK && data != null) {
            Place place = Autocomplete.getPlaceFromIntent(data);
            Log.d(TAG, "place name: " + place.getName());
            View view = findViewById(R.id.main);
            Snackbar.make(view, "PLACE NAME: " + place.getName(), Snackbar.LENGTH_SHORT)
                    .setAction("Action", null).show();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void launchAutoComplete(AutocompleteActivityMode mode) {
        try {
            // autocomplete activity method
            // https://developers.google.com/places/android-sdk/autocomplete#option_2_use_an_intent_to_launch_the_autocomplete_activity
            Intent intent = getAutocompleteIntent(this, mode);
            // result will be received in onActivityResult()
            startActivityForResult(intent, AUTOCOMPLETE_REQUEST_CODE);
        } catch (Exception e) {
            // TODO: Handle the error.
        }

    }

    static Intent getAutocompleteIntent(Activity activity, AutocompleteActivityMode mode) {
        // https://developers.google.com/places/android-sdk/autocomplete
        // using Autocomplete Activity method (option 2):
        // https://developers.google.com/places/android-sdk/autocomplete#option_2_use_an_intent_to_launch_the_autocomplete_activity

        if (!Places.isInitialized()) {
            Places.initialize(activity.getApplicationContext(), PLACES_API_KEY);
        }

        List<Place.Field> fields = Arrays.asList(Place.Field.LAT_LNG, Place.Field.NAME, Place.Field.ADDRESS, Place.Field.ADDRESS_COMPONENTS);
        return new Autocomplete.IntentBuilder(mode, fields)
                .build(activity);

    }

}
